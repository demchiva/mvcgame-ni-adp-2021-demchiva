package cz.cvut.fit.miadp.mvcgame.command;

import cz.cvut.fit.miadp.mvcgame.model.IGameModel;

public class SwitchGameFactoryCmd extends AbstractGameCommand {
    public SwitchGameFactoryCmd(IGameModel model) {
        this.subject = model;
    }

    @Override
    protected void execute() {
        this.subject.changeFamilyGameObjects();
    }
}
