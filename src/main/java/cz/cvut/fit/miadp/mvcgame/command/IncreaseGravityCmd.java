package cz.cvut.fit.miadp.mvcgame.command;

import cz.cvut.fit.miadp.mvcgame.model.IGameModel;

public class IncreaseGravityCmd extends AbstractGameCommand {
    public IncreaseGravityCmd(IGameModel model) {
        this.subject = model;
    }

    @Override
    protected void execute() {
        this.subject.increaseGravity();
    }
}
