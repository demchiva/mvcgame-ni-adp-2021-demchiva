package cz.cvut.fit.miadp.mvcgame.command;

import cz.cvut.fit.miadp.mvcgame.model.IGameModel;

public class SpawnEnemyCmd extends AbstractGameCommand {
    public SpawnEnemyCmd(IGameModel model) {
        this.subject = model;
    }

    @Override
    protected void execute() {
        this.subject.spawnEnemy();
    }
}
