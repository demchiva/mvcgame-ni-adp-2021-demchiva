package cz.cvut.fit.miadp.mvcgame.model.gameObjects.family_B;

import cz.cvut.fit.miadp.mvcgame.config.MvcGameConfig;
import cz.cvut.fit.miadp.mvcgame.model.Position;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbsMissile;
import cz.cvut.fit.miadp.mvcgame.strategy.IMovingStrategy;

import java.util.Random;

public class Missile_B extends AbsMissile {
    IMovingStrategy movingStrategy;
    
    public Missile_B(Position initialPosition, double initAngle, int initVelocity, IMovingStrategy movingStrategy) {
        super(initialPosition, initAngle, initVelocity);
        this.movingStrategy = movingStrategy;
    }

    @Override
    public void move() {
        initAngle = ((Math.abs(Math.random()) * (MvcGameConfig.MAX_ANGLE - MvcGameConfig.INIT_ANGLE)) + MvcGameConfig.INIT_ANGLE) - MvcGameConfig.ANGLE_STEP * 5;
        this.movingStrategy.updatePosition(this);
    }
}
