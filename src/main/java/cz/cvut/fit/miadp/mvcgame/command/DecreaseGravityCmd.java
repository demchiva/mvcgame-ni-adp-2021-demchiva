package cz.cvut.fit.miadp.mvcgame.command;

import cz.cvut.fit.miadp.mvcgame.model.IGameModel;

public class DecreaseGravityCmd extends AbstractGameCommand {
    public DecreaseGravityCmd(IGameModel model) {
        this.subject = model;
    }

    @Override
    protected void execute() {
        this.subject.decreaseGravity();
    }
}
