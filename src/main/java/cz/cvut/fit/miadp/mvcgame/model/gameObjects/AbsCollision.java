package cz.cvut.fit.miadp.mvcgame.model.gameObjects;

import cz.cvut.fit.miadp.mvcgame.model.Position;
import cz.cvut.fit.miadp.mvcgame.model.Soundable;
import cz.cvut.fit.miadp.mvcgame.visitor.IGameObjectsVisitor;

public abstract class AbsCollision extends LifetimeLimitedGameObject implements Soundable {
    protected AbsEnemy enemy;
    protected AbsMissile missile;

    public AbsCollision(Position position, AbsEnemy enemy, AbsMissile missile) {
        super(position);
        doSound();
        this.enemy = enemy;
        this.missile = missile;
    }

    @Override
    public String getSoundFileName() {
        return "src/main/resources/sounds/collision.mp3";
    }

    @Override
    public void acceptVisitor(IGameObjectsVisitor visitor) {
        visitor.visitCollision(this);
    }
}
