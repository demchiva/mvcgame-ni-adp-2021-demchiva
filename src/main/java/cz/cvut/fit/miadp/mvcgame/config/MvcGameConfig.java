package cz.cvut.fit.miadp.mvcgame.config;

public class MvcGameConfig {
    public static final int MAX_X = 1280;
    public static final int MAX_Y = 720;
	public static final int MOVE_STEP = 10;
	public static final int CANNON_POS_X = 50;
	public static final int CANNON_POS_Y = MAX_Y / 2;
	public static final double GRAVITY = 9.8;
	public static final double GRAVITY_STEP = 0.1;
	public static final int INIT_POWER = 10;
	public static final double INIT_ANGLE = 0;
	public static final double MAX_ANGLE = Math.PI / 2;
	public static final double ANGLE_STEP = Math.PI / 18;
	public static final int POWER_STEP = 10;
	public static final int INFO_OUTLINE = 20;
	public static final int CANNON_HEIGHT = 69;
	public static final int LEFT_X_ENEMY_SPAWN_BOUNDARY = 60;
	public static final int RIGHT_X_ENEMY_SPAWN_BOUNDARY = MvcGameConfig.MAX_X-100;
	public static final int LEFT_Y_ENEMY_SPAWN_BOUNDARY = 60;
	public static final int RIGHT_Y_ENEMY_SPAWN_BOUNDARY = MvcGameConfig.MAX_Y-100;
	public static final int ENEMY_WIDTH = 30;
	public static final int ENEMY_HEIGHT = 29;
	public static final int MISSILE_WIDTH = 30;
	public static final int MISSILE_HEIGHT = 29;
	public static final double COLLISION_DISTANCE = 15;
}