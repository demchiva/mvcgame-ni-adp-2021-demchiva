package cz.cvut.fit.miadp.mvcgame.model.gameObjects.family_B;

import cz.cvut.fit.miadp.mvcgame.model.Position;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbsEnemy;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.IPath;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.SimplePath;

public class Enemy_B extends AbsEnemy {
    private IPath enemyPath;

    public Enemy_B(Position position) {
        super(position, "images/enemy2.png");
        this.enemyPath = new SimplePath(this.position);
    }

    @Override
    public void die() {
        isAlive = false;
    }

    @Override
    public void move() {
        this.position = enemyPath.getNextPosition();
    }
}
