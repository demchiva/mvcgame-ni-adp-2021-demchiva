package cz.cvut.fit.miadp.mvcgame.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.Stack;
import java.util.concurrent.LinkedBlockingQueue;

import cz.cvut.fit.miadp.mvcgame.abstractfactory.GameObjectsFactory_A;
import cz.cvut.fit.miadp.mvcgame.abstractfactory.GameObjectsFactory_B;
import cz.cvut.fit.miadp.mvcgame.abstractfactory.IGameObjectFactory;
import cz.cvut.fit.miadp.mvcgame.command.AbstractGameCommand;
import cz.cvut.fit.miadp.mvcgame.config.MvcGameConfig;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.*;
import cz.cvut.fit.miadp.mvcgame.observer.IObserver;
import cz.cvut.fit.miadp.mvcgame.state.DoubleShootingMode;
import cz.cvut.fit.miadp.mvcgame.state.IShootingMode;
import cz.cvut.fit.miadp.mvcgame.strategy.IMovingStrategy;
import cz.cvut.fit.miadp.mvcgame.strategy.RealisticMovingStrategy;
import cz.cvut.fit.miadp.mvcgame.strategy.SimpleMovingStrategy;

public class GameModel implements IGameModel {
    private AbsCannon cannon;
    private List<AbsMissile> missiles;
    private List<AbsEnemy> enemies;
    private List<AbsCollision> collisions;

    private List<IObserver> observers;
    private IGameObjectFactory goFact;
    private IMovingStrategy movingStrategy;

    private int score;
    private double gravity;

    private Queue<AbstractGameCommand> unexecutedCmds = new LinkedBlockingQueue<>();
    private Stack<AbstractGameCommand> executedCmds = new Stack<>();

    public GameModel() {
        this.movingStrategy = new SimpleMovingStrategy();
        this.observers = new ArrayList<>();
        this.missiles = new ArrayList<>();
        goFact = GameObjectsFactory_A.getInstance();
        goFact.setModel(this);
        this.cannon = goFact.createCannon();
        this.score = 0;
        gravity = MvcGameConfig.GRAVITY;
        this.enemies = new ArrayList<>();
        this.collisions = new ArrayList<>();
    }

    public void moveCannonUp() {
        this.cannon.moveUp();
        this.notifyObservers();
    }

    public void moveCannonDown() {
        this.cannon.moveDown();
        this.notifyObservers();
    }

    public void cannonShoot() {
        this.missiles.addAll(cannon.shoot());
        this.notifyObservers();
    }

    public List<GameObject> getGameObjects() {
        List<GameObject> go = new ArrayList<>(this.missiles);
        go.add(this.cannon);
        go.add(getGameInfo());
        go.addAll(enemies);
        go.addAll(collisions);
        return go;
    }

    public void update() {
        this.executeCmds();
        this.moveEnemies();
        this.moveMissiles();
        this.checkCollisions();
        this.destroyCollisions();
        this.destroyEnemies();
    }

    private void moveEnemies() {
        for(AbsEnemy enemy: enemies) {
            enemy.move();
        }
    }

    private void destroyEnemies() {
        List<AbsEnemy> toRemove = new ArrayList<>();
        for(AbsEnemy enemy : enemies) {
            if(!enemy.isAlive())
                toRemove.add(enemy);
        }
        enemies.removeAll(toRemove);
    }

    private void destroyCollisions() {
        List<AbsCollision> toRemove = new ArrayList<>();
        for(AbsCollision collision : collisions) {
            if(collision.getAge() > 1000)
                toRemove.add(collision);
        }
        collisions.removeAll(toRemove);
    }

    private void checkCollisions() {
        for (AbsEnemy enemy: enemies) {
            for(AbsMissile missile : missiles) {
                if(distanceBetweenMissileAndEnemy(missile,enemy) <= MvcGameConfig.COLLISION_DISTANCE) {
                    enemy.die();
                    if(!enemy.isAlive()) {
                        this.collisions.add(this.goFact.createCollision(enemy.getPosition(), enemy, missile));
                    }
                    this.score++;
                }
            }
        }
    }

    private double distanceBetweenMissileAndEnemy(AbsMissile missile, AbsEnemy enemy) {
        int x1 = (missile.getPosition().getX() + MvcGameConfig.MISSILE_WIDTH)/2;
        int y1 = (missile.getPosition().getY() + MvcGameConfig.MISSILE_HEIGHT)/2;
        int x2 = (enemy.getPosition().getX() + MvcGameConfig.ENEMY_WIDTH)/2;
        int y2 = (enemy.getPosition().getY() + MvcGameConfig.ENEMY_HEIGHT)/2;
        return Math.sqrt((y2 - y1) * (y2 - y1) + (x2 - x1) * (x2 - x1));
    }

    private void destroyMissiles() {
        List<AbsMissile> toRemove = new ArrayList<>();
        for (AbsMissile missile : this.missiles) {
            if (missile.getPosition().getX() > MvcGameConfig.MAX_X) {
                toRemove.add(missile);
            }
        }

        this.missiles.removeAll(toRemove);
    }

    private void moveMissiles() {
        for (AbsMissile missile : this.missiles) {
            missile.move();
        }

        this.destroyMissiles();
        this.notifyObservers();
    }

    @Override
    public void registerObserver(IObserver obs) {
        if (!this.observers.contains(obs)) {
            this.observers.add(obs);
        }
    }

    @Override
    public void unregisterObserver(IObserver obs) {
        this.observers.remove(obs);
    }

    @Override
    public void notifyObservers() {
        for (IObserver obs : this.observers) {
            obs.update();
        }
    }

    public IMovingStrategy getMovingStrategy() {
        return this.movingStrategy;
    }

    public void aimCannonUp() {
        this.cannon.aimUp();
        this.notifyObservers();
    }

    public void aimCannonDown() {
        this.cannon.aimDown();
        this.notifyObservers();
    }

    public void cannonPowerUp() {
        this.cannon.powerUp();
        this.notifyObservers();
    }
    
    public void cannonPowerDown() {
        this.cannon.powerDown();
        this.notifyObservers();
    }

    public void toggleMovingStrategy() {
        if(this.movingStrategy instanceof SimpleMovingStrategy) {
            this.movingStrategy = new RealisticMovingStrategy();
        } else if(this.movingStrategy instanceof RealisticMovingStrategy) {
            this.movingStrategy = new SimpleMovingStrategy();
        }
        this.movingStrategy.setGravity(gravity);
    }

    private static class Memento {
        private int score;
        private int cannonX;
        private int cannonY;
        private int cannonPower;
        private double gravity;
        private double cannonAngle;
        private IMovingStrategy movingStrategy;
        private IShootingMode shootingMode;
        private IGameObjectFactory objectFactory;
        private List<Position> enemies;
    }

    public Object createMemento() {
        Memento m = new Memento();
        m.enemies = new ArrayList<>();
        m.score = this.score;
        m.cannonX = this.getCannonPosition().getX();
        m.cannonY = this.getCannonPosition().getY();
        m.cannonAngle = this.cannon.getAngle();
        m.cannonPower = this.cannon.getPower();
        m.movingStrategy = this.movingStrategy;
        m.shootingMode = this.cannon.getShootingMode();
        m.objectFactory = this.goFact;
        m.gravity = this.gravity;
        for(AbsEnemy enemy : this.enemies) {
            m.enemies.add(enemy.getStartingPosition());
        }
        return m;
    }

    public void setMemento(Object memento) {
        Memento m = (Memento) memento;
        this.score = m.score;
        this.goFact = m.objectFactory;
        this.cannon = this.goFact.createCannon();
        this.cannon.getPosition().setY(m.cannonY);
        this.cannon.getPosition().setX(m.cannonX);
        this.cannon.setAngle(m.cannonAngle);
        this.cannon.setPower(m.cannonPower);
        this.movingStrategy = m.movingStrategy;
        this.enemies.clear();
        for(Position position : m.enemies) {
            this.spawnEnemyAtPosition(position);
        }
        this.gravity = m.gravity;
        if(m.shootingMode instanceof DoubleShootingMode) {
            this.cannon.toggleShootingMode();
        }
    }

    private void spawnEnemyAtPosition(Position position) {
        this.enemies.add(this.goFact.createEnemy(new Position(position.getX(), position.getY())));
    }

    public Position getCannonPosition() {
        return new Position(this.cannon.getPosition().getX(), this.cannon.getPosition().getY());
    }

    public void registerCommand(AbstractGameCommand cmd) {
        this.unexecutedCmds.add(cmd);
    }

    public void undoLastCommand() {
        if(this.executedCmds.isEmpty()) return;

        AbstractGameCommand cmd = this.executedCmds.pop();
        cmd.unExecute();

        this.notifyObservers();
    }

    private void executeCmds() {
        while(!this.unexecutedCmds.isEmpty()) {
            AbstractGameCommand cmd = this.unexecutedCmds.poll();
            cmd.doExecute();
            this.executedCmds.push(cmd);
        }
    }

    @Override
    public void toggleShootingState() {
        this.cannon.toggleShootingMode();
    }

    @Override
    public void increaseGravity() {
        gravity += MvcGameConfig.GRAVITY_STEP;
        this.movingStrategy.setGravity(gravity);
    }

    @Override
    public void decreaseGravity() {
        gravity -= MvcGameConfig.GRAVITY_STEP;
        this.movingStrategy.setGravity(gravity);
    }

    @Override
    public void changeFamilyGameObjects() {
        if (goFact instanceof GameObjectsFactory_A) {
            goFact = GameObjectsFactory_B.getInstance();
        } else if (goFact instanceof GameObjectsFactory_B) {
            goFact = GameObjectsFactory_A.getInstance();
        }
        goFact.setModel(this);
        Position canonPosition = new Position(this.cannon.getPosition().getX(), this.cannon.getPosition().getY());
        this.cannon = this.goFact.createCannon();
        this.cannon.getPosition().setY(canonPosition.getY());
        this.cannon.getPosition().setX(canonPosition.getX());
        this.cannon.setAngle(this.cannon.getAngle());
        this.cannon.setPower(this.cannon.getPower());
    }

    private AbsGameInfo getGameInfo() {
        return this.goFact.createGameInfo(this.score, this.cannon.getPower(), this.cannon.getAngle(), this.movingStrategy.getName(), this.cannon.getShootingMode().getName(), this.gravity);
    }

    private int getRandomNumber(int min, int max) {
        return (int) ((Math.random() * (max - min)) + min);
    }

    @Override
    public void spawnEnemy() {
        this.enemies.add(this.goFact.createEnemy(new Position(
                getRandomNumber(MvcGameConfig.LEFT_X_ENEMY_SPAWN_BOUNDARY, MvcGameConfig.RIGHT_X_ENEMY_SPAWN_BOUNDARY),
                getRandomNumber(MvcGameConfig.LEFT_Y_ENEMY_SPAWN_BOUNDARY, MvcGameConfig.RIGHT_Y_ENEMY_SPAWN_BOUNDARY))));
    }
}
