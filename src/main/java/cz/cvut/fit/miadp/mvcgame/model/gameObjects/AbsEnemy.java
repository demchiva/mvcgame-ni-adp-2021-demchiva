package cz.cvut.fit.miadp.mvcgame.model.gameObjects;

import cz.cvut.fit.miadp.mvcgame.model.Position;
import cz.cvut.fit.miadp.mvcgame.visitor.IGameObjectsVisitor;

public abstract class AbsEnemy extends GameObject {
    protected String path;
    protected boolean isAlive;
    protected Position startingPosition;

    public AbsEnemy(Position position, String path) {
        this.position = position;
        this.startingPosition = new Position(position.getX(), position.getY());
        this.isAlive = true;
        this.path = path;
    }

    public Position getStartingPosition() { return this.startingPosition;}
    public boolean isAlive() {
        return isAlive;
    }
    public abstract void die();
    public abstract void move();

    @Override
    public void acceptVisitor(IGameObjectsVisitor render) {
        if(this.isAlive) {
            render.visitEnemy(this, path);
        }
    }
}
