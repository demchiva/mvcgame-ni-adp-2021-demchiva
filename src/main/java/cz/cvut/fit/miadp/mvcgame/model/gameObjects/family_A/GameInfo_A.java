package cz.cvut.fit.miadp.mvcgame.model.gameObjects.family_A;

import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbsGameInfo;

public class GameInfo_A  extends AbsGameInfo {
    public GameInfo_A(int score, int power, double angle , String shootMode, String missileMode, double gravity) {
        this.power = power;
        this.score = score;
        this.shootMode = shootMode;
        this.missileMode = missileMode;
        this.angle = angle;
        this.gravity = gravity;
    }

    @Override
    public String getText() {
        return "Score: " + this.score +
                " Angle: " + this.angle +
                " Cannon Power: " + this.power +
                " Shooting Mode: " + this.shootMode +
                " Missile Mode: " + this.missileMode +
                " Gravity " + this.gravity;
    }
}
