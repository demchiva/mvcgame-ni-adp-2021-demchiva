package cz.cvut.fit.miadp.mvcgame.strategy;

import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbsMissile;

public interface IMovingStrategy {
    String getName();
    void setGravity(double gravity);
    void updatePosition(AbsMissile missile);
}
