package cz.cvut.fit.miadp.mvcgame.abstractfactory;

import cz.cvut.fit.miadp.mvcgame.model.IGameModel;
import cz.cvut.fit.miadp.mvcgame.model.Position;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.*;

public interface IGameObjectFactory {
    AbsCannon createCannon();
    AbsMissile createMissile(double initAngle, int initVelocity);
    AbsEnemy createEnemy(Position position);
    AbsCollision createCollision(Position position, AbsEnemy enemy, AbsMissile missile);
    AbsGameInfo createGameInfo(int score, int power, double angle , String shootMode, String missileMode, double gravity);
    void setModel(IGameModel model);
}
