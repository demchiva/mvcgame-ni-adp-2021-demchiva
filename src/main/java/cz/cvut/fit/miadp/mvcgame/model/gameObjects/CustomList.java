package cz.cvut.fit.miadp.mvcgame.model.gameObjects;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class CustomList<Type> implements Iterable<Type>{
    private List<Type> list;

    public CustomList() {
        this.list = new ArrayList<>();
    }

    public void add(Type item)
    {
        this.list.add(item);
    }
    @Override
    public Iterator<Type> iterator() {
        return new Iterator<>() {

            private int currentIndex = 0;
            private int direction = -1;
            @Override
            public boolean hasNext() {
                return !list.isEmpty();
            }

            @Override
            public Type next() {
                if(direction == 1) {
                    currentIndex++;
                    if(currentIndex >= list.size()-1) {
                        currentIndex = list.size()-1;
                        direction=-1;
                    }
                }
                else if(direction == -1) {
                    currentIndex--;
                    if(currentIndex <= 0) {
                        currentIndex = 0;
                        direction = 1;
                    }
                }
                return list.get(currentIndex);
            }

            @Override
            public void remove() {
                throw new UnsupportedOperationException();
            }
        };
    }
}
