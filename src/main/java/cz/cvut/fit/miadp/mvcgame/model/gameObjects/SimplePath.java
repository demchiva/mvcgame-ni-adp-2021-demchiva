package cz.cvut.fit.miadp.mvcgame.model.gameObjects;

import cz.cvut.fit.miadp.mvcgame.model.Position;

import java.util.Iterator;

public class SimplePath implements IPath {
    private Iterator<Position> it;

    public SimplePath(Position position) {
        CustomList<Position> path = new CustomList<>();
        this.it = path.iterator();

        for(int i = 0; i < 10; i++) {
            path.add(new Position(position.getX(),position.getY() - (10-i) * 10));
        }

        for(int i = 0; i < 10; i++) {
            path.add(new Position(position.getX(),position.getY() + i * 10));
        }
    }

    public Position getNextPosition()
    {
        return it.next();
    }
}
