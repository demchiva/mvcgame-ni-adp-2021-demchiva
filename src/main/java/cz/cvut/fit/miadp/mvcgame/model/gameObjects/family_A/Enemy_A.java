package cz.cvut.fit.miadp.mvcgame.model.gameObjects.family_A;

import cz.cvut.fit.miadp.mvcgame.model.Position;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbsEnemy;

public class Enemy_A extends AbsEnemy {
    public Enemy_A(Position position) {
        super(position, "images/enemy1.png");
    }

    @Override
    public void die() {
        isAlive = false;
    }

    @Override
    public void move() {}
}
