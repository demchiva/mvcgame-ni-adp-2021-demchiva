package cz.cvut.fit.miadp.mvcgame.abstractfactory;

import cz.cvut.fit.miadp.mvcgame.config.MvcGameConfig;
import cz.cvut.fit.miadp.mvcgame.model.IGameModel;
import cz.cvut.fit.miadp.mvcgame.model.Position;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.*;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.family_B.*;

public class GameObjectsFactory_B implements IGameObjectFactory {
    private IGameModel model;

    private static class SingletonHolder {
        private static final GameObjectsFactory_B INSTANCE = new GameObjectsFactory_B();
    }

    public static GameObjectsFactory_B getInstance() {
        return SingletonHolder.INSTANCE;
    }

    public GameObjectsFactory_B(IGameModel model) {
        this.model = model;
    }

    public GameObjectsFactory_B() {}

    @Override
    public void setModel(IGameModel model) {
        this.model = model;
    }

    @Override
    public AbsCannon createCannon() {
        return new Cannon_B(new Position(MvcGameConfig.CANNON_POS_X, MvcGameConfig.CANNON_POS_Y), this);
    }

    @Override
    public AbsMissile createMissile(double initAngle, int initVelocity) {
        return new Missile_B(this.model.getCannonPosition(), initAngle, initVelocity, this.model.getMovingStrategy());
    }

    @Override
    public AbsEnemy createEnemy(Position position) {
        return new Enemy_B(position);
    }

    @Override
    public AbsCollision createCollision(Position position, AbsEnemy enemy, AbsMissile missile) {
        return new Collision_B(position, enemy, missile);
    }

    @Override
    public AbsGameInfo createGameInfo(int score, int power, double angle, String shootMode, String missileMode, double gravity) {
        return new GameInfo_B(score, power, angle, shootMode, missileMode, gravity);
    }
}
