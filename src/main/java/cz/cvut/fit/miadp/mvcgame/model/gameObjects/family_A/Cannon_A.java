package cz.cvut.fit.miadp.mvcgame.model.gameObjects.family_A;

import java.util.ArrayList;
import java.util.List;

import cz.cvut.fit.miadp.mvcgame.abstractfactory.IGameObjectFactory;
import cz.cvut.fit.miadp.mvcgame.config.MvcGameConfig;
import cz.cvut.fit.miadp.mvcgame.model.Position;
import cz.cvut.fit.miadp.mvcgame.model.Vector;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbsCannon;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbsMissile;
import cz.cvut.fit.miadp.mvcgame.state.IShootingMode;

public class Cannon_A extends AbsCannon {
    private IGameObjectFactory goFact;
    private double angle;
    private int power;
    private List<AbsMissile> shootingBatch;

    public Cannon_A(Position initialPosition, IGameObjectFactory goFact) {
        this.position = initialPosition;
        this.goFact = goFact;

        this.power = MvcGameConfig.INIT_POWER;
        this.angle = MvcGameConfig.INIT_ANGLE;

        this.shootingBatch = new ArrayList<>();
        this.shootingMode = AbsCannon.SINGLE_SHOOTING_MODE;
    }

    @Override
    public void moveUp() {
        this.move(new Vector(0, -1* MvcGameConfig.MOVE_STEP));
        if(this.position.getY() < 0) {
            this.position.setY(0);
        }
    }

    @Override
    public void moveDown() {
        this.move(new Vector(0, MvcGameConfig.MOVE_STEP));
        if(this.position.getY() > (MvcGameConfig.MAX_Y - MvcGameConfig.CANNON_HEIGHT)) {
            this.position.setY(MvcGameConfig.MAX_Y - MvcGameConfig.CANNON_HEIGHT);
        }
    }

    @Override
    public List<AbsMissile> shoot() {
        this.shootingBatch.clear();

        // use current state to shoot
        this.shootingMode.shoot(this);

        return this.shootingBatch;
    }

    public void primitiveShoot() {
        this.shootingBatch.add(this.goFact.createMissile(this.angle, this.power));
    }

    @Override
    public void aimUp() {
        this.angle -= MvcGameConfig.ANGLE_STEP;
    }

    @Override
    public void aimDown() {
        this.angle += MvcGameConfig.ANGLE_STEP;
    }

    @Override
    public void powerUp() {
        this.power += MvcGameConfig.POWER_STEP;
    }

    @Override
    public void powerDown() {
        this.power -= MvcGameConfig.POWER_STEP;
        if(this.power <= 0) {
            this.power = MvcGameConfig.POWER_STEP;
        }
    }

    @Override
    public IShootingMode getShootingMode() {
        return shootingMode;
    }

    @Override
    public double getAngle() {
        return angle;
    }

    @Override
    public int getPower() {
        return power;
    }

    @Override
    public void setAngle(double angle) {
        this.angle = angle;
    }

    @Override
    public void setPower(int power) {
        this.power = power;
    }
}
