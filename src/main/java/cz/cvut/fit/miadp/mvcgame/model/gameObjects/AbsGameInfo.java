package cz.cvut.fit.miadp.mvcgame.model.gameObjects;

import cz.cvut.fit.miadp.mvcgame.config.MvcGameConfig;
import cz.cvut.fit.miadp.mvcgame.model.Position;
import cz.cvut.fit.miadp.mvcgame.visitor.IGameObjectsVisitor;

public abstract class AbsGameInfo extends GameObject {
    protected int power;
    protected int score;
    protected double angle;
    protected String shootMode;
    protected String missileMode;
    protected double gravity;

    public AbsGameInfo() {
        this.position = new Position(0, MvcGameConfig.INFO_OUTLINE);
    }

    public abstract String getText();

    @Override
    public void acceptVisitor(IGameObjectsVisitor visitor) {
        visitor.visitGameInfo(this);
    }
}
