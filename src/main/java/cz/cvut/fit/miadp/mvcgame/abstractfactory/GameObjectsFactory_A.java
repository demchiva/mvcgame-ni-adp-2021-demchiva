package cz.cvut.fit.miadp.mvcgame.abstractfactory;

import cz.cvut.fit.miadp.mvcgame.config.MvcGameConfig;
import cz.cvut.fit.miadp.mvcgame.model.IGameModel;
import cz.cvut.fit.miadp.mvcgame.model.Position;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbsCollision;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbsEnemy;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbsGameInfo;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbsMissile;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.family_A.*;

public class GameObjectsFactory_A implements IGameObjectFactory {
    private IGameModel model;

    private static class SingletonHolder {
        private static final GameObjectsFactory_A INSTANCE = new GameObjectsFactory_A();
    }

    public static GameObjectsFactory_A getInstance() {
        return SingletonHolder.INSTANCE;
    }

    public GameObjectsFactory_A(IGameModel model) {
        this.model = model;
    }

    public GameObjectsFactory_A() {}

    @Override
    public void setModel(IGameModel model) {
        this.model = model;
    }

    @Override
    public Cannon_A createCannon() {
        return new Cannon_A(new Position(MvcGameConfig.CANNON_POS_X, MvcGameConfig.CANNON_POS_Y), this);
    }

    @Override
    public Missile_A createMissile(double initAngle, int initVelocity) {
        return new Missile_A(this.model.getCannonPosition(), initAngle, initVelocity, this.model.getMovingStrategy());
    }

    @Override
    public AbsEnemy createEnemy(Position position) {
        return new Enemy_A(position);
    }

    @Override
    public AbsCollision createCollision(Position position, AbsEnemy enemy, AbsMissile missile) {
        return new Collision_A(position, enemy, missile);
    }

    @Override
    public AbsGameInfo createGameInfo(int score, int power, double angle , String shootMode, String missileMode, double gravity) {
        return new GameInfo_A(score, power, angle , shootMode, missileMode, gravity);
    }
}
