package cz.cvut.fit.miadp.mvcgame.model.gameObjects.family_B;

import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbsGameInfo;

public class GameInfo_B extends AbsGameInfo {
    public GameInfo_B(int score, int power, double angle , String shootMode, String missileMode, double gravity) {
        this.power = power;
        this.score = score;
        this.shootMode = shootMode;
        this.missileMode = missileMode;
        this.angle = angle;
        this.gravity = gravity;
    }

    @Override
    public String getText() {
        return "Score: " + this.score;
    }
}
