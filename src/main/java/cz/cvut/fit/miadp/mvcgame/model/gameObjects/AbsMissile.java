package cz.cvut.fit.miadp.mvcgame.model.gameObjects;

import cz.cvut.fit.miadp.mvcgame.model.Position;
import cz.cvut.fit.miadp.mvcgame.model.Soundable;
import cz.cvut.fit.miadp.mvcgame.visitor.IGameObjectsVisitor;

public abstract class AbsMissile extends LifetimeLimitedGameObject implements Soundable {
    protected double initAngle;
    protected int initVelocity;

    protected AbsMissile(Position initialPosition, double initAngle, int initVelocity) {
        super(initialPosition);
        doSound();
        this.initAngle = initAngle;
        this.initVelocity = initVelocity;
    }

    @Override
    public String getSoundFileName() {
        return "src/main/resources/sounds/shoot.mp3";
    }

    @Override
    public void acceptVisitor(IGameObjectsVisitor visitor) {
        visitor.visitMissile(this);
    }

	public int getInitVelocity() {
		return this.initVelocity;
	}

	public double getInitAngle() {
		return this.initAngle;
    }
    
    public abstract void move();
}
