package cz.cvut.fit.miadp.mvcgame.visitor;

import cz.cvut.fit.miadp.mvcgame.bridge.IGameGraphics;
import cz.cvut.fit.miadp.mvcgame.config.MvcGameConfig;
import cz.cvut.fit.miadp.mvcgame.model.Position;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.*;

public class GameObjectsRender implements IGameObjectsVisitor {
    private IGameGraphics gr;

    public void setGraphicContext(IGameGraphics gr)
    {
        this.gr = gr;
    }

    @Override
    public void visitCannon(AbsCannon cannon) {
        this.gr.drawImage("images/cannon.png", cannon.getPosition());
    }

    @Override
    public void visitMissile(AbsMissile missile) {
        this.gr.drawImage("images/missile.png", missile.getPosition());
    }

    @Override
    public void visitEnemy(AbsEnemy enemy, String path) {
        this.gr.drawImage(path, enemy.getPosition());
    }

    @Override
    public void visitCollision(AbsCollision collision) {
        this.gr.drawImage("images/collision.png", collision.getPosition());
    }

    @Override
    public void visitGameInfo(AbsGameInfo gameInfo) {
        this.gr.drawText(gameInfo.getText(), new Position(MvcGameConfig.MAX_X / 2, 10));
    }
}
