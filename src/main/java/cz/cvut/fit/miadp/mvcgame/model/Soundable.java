package cz.cvut.fit.miadp.mvcgame.model;

import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;

import java.io.File;

/**
 * The Template method pattern.
 */
public interface Soundable {
    public default void doSound() {
        new Thread(() -> {
            try {
                Media media = new Media(new File(getSoundFileName()).toURI().toString());
                MediaPlayer mediaPlayer = new MediaPlayer(media);
                mediaPlayer.play();
            } catch (Exception e) {
                System.err.println(e.getMessage());
            }
        }).start();
    }

    public String getSoundFileName();
}
