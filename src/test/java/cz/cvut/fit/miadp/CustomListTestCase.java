package cz.cvut.fit.miadp;

import cz.cvut.fit.miadp.mvcgame.model.Position;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.CustomList;
import org.junit.Test;

import java.util.Iterator;

import static junit.framework.Assert.assertEquals;

public class CustomListTestCase {

    @Test
    public void testIterator() {
        CustomList<Position> list = new CustomList<>();
        Position position1 = new Position(0,0);
        list.add(position1);
        Iterator<Position> it = list.iterator();
        assertEquals(it.next(), position1);
        Position position2 = new Position(1,1);
        list.add(position2);
        assertEquals(it.next(),position2);
        assertEquals(it.next(),position1);
    }
}
