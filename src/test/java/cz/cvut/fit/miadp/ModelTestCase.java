package cz.cvut.fit.miadp;

import cz.cvut.fit.miadp.mvcgame.command.MoveCannonUpCmd;
import cz.cvut.fit.miadp.mvcgame.model.GameModel;
import cz.cvut.fit.miadp.mvcgame.model.IGameModel;
import cz.cvut.fit.miadp.mvcgame.model.Position;
import org.junit.Assert;
import org.junit.Test;

public class ModelTestCase {

    @Test
    public void UndoCheck() {
        IGameModel model =  new GameModel();

        Position positionBeforeUndo = new Position(model.getCannonPosition().getX(), model.getCannonPosition().getY());

        model.registerCommand(new MoveCannonUpCmd(model));
        model.update();
        model.undoLastCommand();

        Position positionAfterUndo = model.getCannonPosition();

        Assert.assertEquals(positionBeforeUndo.getX(), positionAfterUndo.getX());
        Assert.assertEquals(positionBeforeUndo.getY(), positionAfterUndo.getY());
    }


}
