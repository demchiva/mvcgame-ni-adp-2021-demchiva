package cz.cvut.fit.miadp;

import cz.cvut.fit.miadp.mvcgame.command.MoveCannonUpCmd;
import cz.cvut.fit.miadp.mvcgame.controller.GameController;
import cz.cvut.fit.miadp.mvcgame.model.GameModel;
import cz.cvut.fit.miadp.mvcgame.model.IGameModel;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

public class ControllerTestCase {

    @Test
    public void checkCommandCalling() {
        IGameModel model =  mock(GameModel.class);
        GameController controller = new GameController(model);
        List<String> map = new ArrayList<>();
        map.add("UP");
        controller.processPressedKeys(map);
        verify(model, times(1)).registerCommand(any(MoveCannonUpCmd.class));
    }
}
