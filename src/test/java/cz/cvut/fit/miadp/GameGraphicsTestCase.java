package cz.cvut.fit.miadp;

import cz.cvut.fit.miadp.mvcgame.bridge.GameGraphics;
import cz.cvut.fit.miadp.mvcgame.bridge.IGameGraphics;
import cz.cvut.fit.miadp.mvcgame.model.GameModel;
import cz.cvut.fit.miadp.mvcgame.model.IGameModel;
import cz.cvut.fit.miadp.mvcgame.model.Position;
import cz.cvut.fit.miadp.mvcgame.view.GameView;
import org.junit.Test;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;

public class GameGraphicsTestCase {

    @Test
    public void TestGraphics() {
        IGameGraphics gr = mock(GameGraphics.class);
        IGameModel model = new GameModel();
        GameView view = new GameView(model);
        view.setGraphicContext(gr);
        view.render();
        verify(gr, times(1)).drawImage(anyString(), any(Position.class));
        verify(gr, times(1)).drawText(anyString(), any(Position.class));
    }
}
