package cz.cvut.fit.miadp;

import cz.cvut.fit.miadp.mvcgame.model.GameModel;
import cz.cvut.fit.miadp.mvcgame.model.IGameModel;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbsMissile;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.GameObject;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.family_B.Missile_B;
import org.junit.Test;

import static junit.framework.Assert.assertEquals;

public class CannonTestCase {

    @Test
    public void checkMissileClass() {
        IGameModel model = new GameModel();
        model.changeFamilyGameObjects();
        model.cannonShoot();
        for(GameObject object : model.getGameObjects()) {
            assert !(object instanceof AbsMissile) || (object instanceof Missile_B);
        }
    }

    @Test
    public void checkMissileCountSingleShoot() {
        IGameModel model = new GameModel();
        model.cannonShoot();
        int numberOfMissiles = 0;
        for(GameObject object : model.getGameObjects()) {
            if(object instanceof AbsMissile) {
                numberOfMissiles++;
            }
        }
        assertEquals (numberOfMissiles,1);
    }

    @Test
    public void checkMissileCountDoubleShoot() {
        IGameModel model = new GameModel();
        model.toggleShootingState();
        model.cannonShoot();
        int numberOfMissiles = 0;
        for(GameObject object : model.getGameObjects()) {
            if(object instanceof AbsMissile) {
                numberOfMissiles++;
            }
        }
        assertEquals (numberOfMissiles,2);
    }
}
